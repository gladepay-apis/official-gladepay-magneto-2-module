# GladePay Magento 2 Module

Magento2 extension for GladePay payment gateway

# Install

- Go to Magento2 root folder

- Enter following command to install module:

```bash
composer require glade/magento_checkout
```

- Wait while dependencies are updated.

- Enter following commands to enable module:

```bash
php bin/magento module:enable Gladepay_Checkout --clear-static-content
php bin/magento setup:upgrade
php bin/magento setup:di:compile
```

- Enable and configure `Gladepay` in _Magento Admin_ under `Stores/Configuration/Payment` Methods
