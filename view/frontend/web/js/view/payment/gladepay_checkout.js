/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'gladepay_checkout',
                component: 'Gladepay_Checkout/js/view/payment/initiator/gladepay_checkout'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);
