<?php

namespace Gladepay\Checkout\Model;

use Magento\Payment\Helper\Data as PaymentHelper;
use Exception;

class Payment implements \Gladepay\Checkout\Api\PaymentInterface
{
    const CODE = 'gladepay_checkout';

    protected $config;
    protected $client_key;
    protected $client_secret;
    protected $endpoint = "https://demo.api.gladepay.com/payment";

    /**
     * @var EventManager
     */
    private $eventManager;

    public function __construct(
        PaymentHelper $paymentHelper,
        \Magento\Framework\Event\Manager $eventManager
    ) {
        $this->eventManager = $eventManager;
        $this->config = $paymentHelper->getMethodInstance(self::CODE);

        $this->client_mid = $this->config->getConfigData('client_mid');
        $this->client_key = $this->config->getConfigData('client_key');

        if ($this->config->getConfigData('go_live')) {
            $this->endpoint = "https://api.gladepay.com/payment";
        }
    }

    /**
     * @return bool
     */
    public function verifyPayment($ref_quote)
    {

        // we are appending quoteid
        $ref = explode('_-~-_', $ref_quote);
        $txnRef = $ref[0];
        $quoteId = $ref[1];

        try {

            $data = [
                'action' => 'verify',
                'txnRef' => $txnRef
            ];

            $curl = curl_init();

            //call the requery instanly to confirm that the transaction was successful
            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->endpoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 300,
                CURLOPT_ENCODING => "",
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "key: {$this->client_key}",
                    "mid: {$this->client_mid}",
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                //$query = "Error #:" . $err;
                return json_encode([
                    'status' => false,
                    'message' => ($query['message'] != null) ? "Error #:" . $err : "Transaction Failed"
                ]);
            } else {
                $query = json_decode($response, true);
            }

            if ($query['txnStatus'] == 'successful') {
                //dispatch the `payment_verify_after` event to update the order status
                $this->eventManager->dispatch('gp_payment_verify_after');

                return json_encode([
                    'status' => true,
                    'message' => "Transaction Approved"
                ]);
            } else {
                return json_encode([
                    'status' => false,
                    'message' => ($query['message'] != null) ? $query['message'] : "Transaction Failed"
                ]);
            }
        } catch (Exception $e) {
            return json_encode([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }

        return json_encode([
            'status' => false,
            'message' => "quote Id is not match"
        ]);
    }
}
